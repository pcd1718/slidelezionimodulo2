# Programmazione Concorrente e Distribuita 2017

Questo repository contiene le slide ed il codice di esempio discusso a lezione per il corso di Programmazione Concorrente e Distribuita per l'A.A. 2017 dell'Università di Padova, Facoltà di Matematica, Corso di Laurea di Informatica (Ordinamento 2011).

Il repository è così organizzato:

* `README.md` : questo file
* `src` : codice sorgente presentato a lezione
* `build.gradle`, `settings.gradle` : istruzioni per la compilazione con lo strumento (Gradle)[https://gradle.org] di cui è richiesta l'installazione.
* `config` : configurazioni per gli strumenti di analisi statica del codice
* `slides` : slide delle lezioni
* `papers` : documenti citati a lezione liberamente distribuibili o link interessanti.

Per predisporre l'esecuzione dei programmi d'esempio con l'IDE Eclipse, usare il comando `gradle eclipse`. Per usare l'IDE JIdea usare il comando `gradle idea`.

# Codice presentato a lezione

Il codice presentato a lezione si trova nei seguenti package:

* `pcd2017.threads.lesson1` - Esempi di concorrenza
* `pcd2017.threads.lesson2` - Executors
* `pcd2017.threads.lesson3` - Locks
* `pcd2017.threads.lesson4` - Concurrent Data Structures
* `pcd2017.threads.lesson5` - Streams
* `pcd2017.threads.lesson5` - Approcci moderni (ReactiveX)
* `pcd2017.distribution.lesson8` - Primitive di Networking
* `pcd2017.distribution.lesson9` - Channels

# Laboratorio 1 - Concorrenza

Il laboratorio 1 è costituito da un insieme di classi di base che vanno completate per ottenere un risultato.

Il codice iniziale si trova nel package `pcd2017.threads.lab1`. E' costituito da alcune classi e relativi test.

Il tema dell'esercizio è la realizzazione di un calcolo parallelo che estragga delle statistiche riguardanti i migliori giocatori di Bowling e le migliori sale da Bowling degli USA.
I dati di partenza sono costituiti da circa 280.000 righe divise in sette file compressi con il compressore XZ. I file vanno letti, vanno calcolati i punteggi ed attribuiti a giocatori e sale, vanno sommati e ordinati per ottenere i 10 migliori giocatori in termini di media punti e le 10 migliori sale in termini di media di strike per partita.

## Package `pcd2017.threads.lab1.bowling`

In questo package sono presenti la classe `GameRecord` che trasporta i risultati di una partita, e la classe `BowlingGame` che è in grado, data la trascrizione di una partita, di calcolare i relativi punteggi e statistiche.

La classe `BowlingGame` è accuratamente testata (proviene da una esercitazione Kata). Può essere un ottimo esercizio usare i test per provare a realizzare una propria implementazione.

## Package `pcd2017.threads.lab1.data`

In questo package si trovano le due classi `DataRecord` e `FinalRecord` suggerite come rappresentazione intermedia e finale dei dati trattati.

## Package `pcd2017.threads.lab1`

Nel package principale si trovano:

* la classe `ScoreReader` che legge i file di ingresso. Vanno completati i metodi mancanti in modo che possa correttamente leggere ed interpretare una riga di input.
* la classe `Summarizer` che implementa una raccolta dati mantenendo una mappa che collega una chiave con un array di interi facilmente mutabile. Va completato il codice che raccoglie effettivamente il dato e lo somma con il parziale finora mantenuto.
* la classe `GameRecordToData` che deve trasformare un record letto in un dato da sommare. Va completata la logica della trasformazione.
* la classe `Main` dove va realizzato il grosso dell'algoritmo. I commenti presenti sono un suggerimento di implementazione; tuttavia, l'unico vincolo è quello di realizzare un algoritmo che in modo parallelo legga i dati dai file di input e li sommi, per poi ordinarli per la presentazione.

Si consiglia, come prima cosa, di completare i test unitari presenti prima di affrontare la struttura del programma principale.

Sono considerati argomento del laboratorio gli strumenti e le classi presentate nelle prime quattro lezioni. Non è necessario per la soluzione l'impiego di Stream. Non è comunque scoraggiato il loro uso da parte di chi si senta in grado di maneggiarli.

E' severamente vietato alterare le classi di test.

# Laboratorio 2 - Distribuzione

Il laboratorio 2 è costituito da un client ed un server che comunicano fra loro in una rete locale.

Il codice inziale è contenuto nel package `pcd2017.distribution.lab2`. Nel package `pcd2017.distribution.lab2.bowling` sono contenute delle classi di supporto.

Il tema dell'esercizio è la realizzazione di un totalizzatore che raccoglie i risultati delle partiti di bowling che si svolgono all'interno della stessa sala. Essendo la rete chiusa e gestita, e la comunicazione infrequente, si considera sufficiente la comunicazione via Datagram UDP, senza connessioni permanenti.

Il protocollo di comunicazione è costituito da messaggi nel formato "<lane>:<pins>" con _lane_ e _pins_ entrambi numeri interi. Per es: "4:5", "1:10", "7:0". _lane_ indica la pista che ha rilevato il tiro, e _pins_ il numero di birilli abbattuti. Il controllo della coerenza del risultato non è importante ed è comunque demandato alla classe che fa la totalizzazione (che viene fornita).

Non è richiesto, nel tema del laboratorio, un controllo o una gestione dell'inizio di una nuova partita sulla stessa _lane_ dopo il completamento della precendente. E' comunque una possibile estensione se il tempo lo permette o come esercizio.

## Package `pcd2017.distribution.lab2.bowling`

Questo package contiene due classi di utilitò:

* la classe `BowlingScorer`, versione modificata di quella usata nel precedente laboratorio, permette di ottenere il punteggio progressivo via via che la partita prosegue.
* la classe `Bowler` genera una partita giocata da un giocatore di un livello fornito. Il parametro del livello regola la bontà dei tiri fatti. La classe si preoccupa di generare una partita valida.

## Classe `pcd2017.distribution.lab2.BowlingClient`

La classe `BowlingClient` è il client. Deve essere completata in modo che predisponga la comunicazione con il server ed invii una serie di risultati per una certa lane.

E' richiesto che la classe:
* generi una partita usando la classe `Bowler`, in modo da inviare ad ogni invocazione dati diversi
* legga dal parametro `args[0]` la _lane_ da inviare nel messaggio
* si concluda dopo aver inviato tutti i dati di una partita
* attenda qualche millisecondo (per es. 500) fra un invio e l'altro, così da poter provare il funzionamento di client concorrenti

La classe deve essere eseguita con il comando:

`gradle -PmainClass=pcd2017.distribution.lab2.BowlingClient -Parg=4 exec`

dove variando il parametro `arg` varia anche la _lane_ emessa nei messaggi.

## Classe `pcd2017.distribution.lab2.BowlingServe`

La classe `BowlingServer` è il server. Deve essere completata in modo che:

* ascolti sulla porta indicata l'arrivo di pacchetti
* ne legga il contenuto per accodarlo ad una `BlockingQueue` di elaborazione

Nello stesso file è definita una classe `ScorePrinter` da completare, il cui scopo è:

* prendere da una `BlockingQueue` un dato da elaborare
* mantenere una mappa delle partite in corso indicizzata per _lane_
* creare una partita se non ancora iniziata per una _lane_
* aggiungere ad una partita iniziata un nuovo risultato preso dalla coda

Si consiglia di esegire con successo il test contenuto in `BowlingServerTest` come prima cosa. Quando il test è verde, procedere all'implementazione del codice di networking.

Sono considerati argomento del laboratorio gli strumenti e le classi presentate fino alla lezione 8 - Primitive di Networking. Non è necessario per la soluzione l'impiego di Channels o asincronia. Non è consigliato intraprendere tale strada durante il laboratorio, sebbene possa essere interessante nello studio personale.

E' perfettamente inutile alterare la classe di test fornita. 

# Comandi utili

## Eseguire una classe contenente un metodo main

Il seguente comando è configurato nel `build.gradle` per eseguire una classe contenente un metodo `public static void main(String[] args)` e passargli un argomento:

`gradle -PmainClass=<complete className> -Parg=<argument> exec` 

Per esempio:

`gradle -PmainClass=pcd2017.distribution.lab2.BowlingClient -Parg=4 exec`

Richiama il metodo `main` della classe `pcd2017.distribution.lab2.BowlingClient` passandogli, in `args[0]`, il valore `"4"`.

Riferimenti: 
* https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Exec.html

## Eseguire un solo test

Il seguente comando esegue un solo test individuato per nome:

`gradle -Dtest.single=<pattern> test`

Il test viene individuato cercando il `pattern` fra i nomi delle classi che contengono test. 

Per esempio, per eseguire la classe di test `pcd2017.threads.lab1gen.BowlerTest` è sufficiente il comando:

`gradle -Dtest.single=BowlerTest test`

Riferimenti: 
* https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html
* https://docs.gradle.org/current/userguide/java_plugin.html#sec:java_test


# Informazioni

Per ogni ulteriore domanda e richiesta di chiarimenti non esitate a contattarmi come indicato a lezione o aprendo una issue su questo repository.

La disponibilità di questo repository sarà garantita almeno fino alla conclusione della sessione d'esami di settembre 2018. Contattatemi per eventuali estensioni o altre necessità.

Buon lavoro,

Michele Mauro