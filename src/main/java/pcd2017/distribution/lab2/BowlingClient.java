package pcd2017.distribution.lab2;

/**
 * Sends to the server data about single throws in the form "<lane>:<score> via datagrams.
 * 
 * Should accept a single param with the number of the lane, and wait a little between datagrams.
 * 
 * Run with: gradle -PmainClass=pcd2017.distribution.lab2.BowlingClient -Parg=4 exec
 * 
 * Try with different numbers as lane.
 * 
 */
public class BowlingClient {
  public static void main(String[] args) {
    // usando la comunicazione via Datagrams
    // prepara un pacchetto per inviare i messaggi
    // prepara i messaggi da inviare
    // per ogni messaggio:
    // formatta il messaggio
    // invia il messaggio
    // attendi qualche centinaio di millisecondi
  }
}
