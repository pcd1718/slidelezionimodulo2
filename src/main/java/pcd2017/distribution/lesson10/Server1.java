package pcd2017.distribution.lesson10;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import pcd2017.distribution.lesson8.Game;
import pcd2017.distribution.lesson8.GameResult;

class NetGame {
  String[] playerId = new String[2];
  private Option<Game> game = Option.none();

  private NetGame(String player1) {
    playerId[0] = player1;
  }

  private NetGame(String player1, String player2) {
    playerId[0] = player1;
    playerId[1] = player2;
    game = Option.of(new Game());
  }

  boolean started() {
    return game.isDefined();
  }

  GameResult status() {
    return game.get().status();
  }

  GameResult move(int player, int move) {
    return game.get().move(player, move);
  }

  /**
   * Add the second player
   * 
   * @param player2
   */
  NetGame match(String player2) {
    return new NetGame(playerId[0], player2);
  }

  static NetGame open(String player1) {
    return new NetGame(player1);
  }
}

class GameServer {

  // active and completed games
  ConcurrentMap<String, NetGame> games = new ConcurrentHashMap<>();

  // awaiting players
  BlockingQueue<NetGame> openings = new LinkedBlockingQueue<>(16);

  private Random random = new Random();

  /**
   * Setup a game
   * 
   * @return the new game ID for the player
   */
  String create() {
    String res;
    if (openings.isEmpty()) {
      // open a new game
      int newId = random.nextInt();
      while (games.containsKey("" + newId))
        newId = random.nextInt();
      res = "" + newId;
      NetGame netGame = NetGame.open(res);
      games.putIfAbsent(res, netGame);
      openings.offer(netGame);
    } else {
      int newId = random.nextInt();
      while (games.containsKey("" + newId))
        newId = random.nextInt();
      res = "" + newId;
      NetGame netGame = openings.poll().match(res);
      games.put(netGame.playerId[0], netGame);
      games.put(netGame.playerId[1], netGame);
    }
    return res;
  }

  /**
   * Returns the status of the game after the move
   * 
   * @param game game where to play; implicitly selects player
   * @param move move to play
   * @return current status of the game
   */
  Option<GameResult> player(String playerId, int move) {
    Option<GameResult> res = Option.none();
    if (games.containsKey(playerId) && games.get(playerId).started()) {
      int idx = games.get(playerId).playerId[0].equals(playerId) ? 0 : 1;
      res = Option.of(games.get(playerId).move(idx, move));
    }
    return res;
  }

  /**
   * True if the player is known
   * 
   * @param playerId
   * @return
   */
  public boolean open(String playerId) {
    return games.containsKey(playerId);
  }

  public Option<Tuple2<Integer, GameResult>> status(String playerId) {
    Option<Tuple2<Integer, GameResult>> res = Option.none();
    if (games.containsKey(playerId) && games.get(playerId).started()) {
      int idx = games.get(playerId).playerId[0].equals(playerId) ? 0 : 1;
      res = Option.of(Tuple.of(idx, games.get(playerId).status()));
    }
    return res;
  }

}

public class Server1 {
  public static void main(String[] args) {

    GameServer gameServer = new GameServer();

    Vertx vertx = Vertx.vertx();
    HttpServerOptions options = new HttpServerOptions().setLogActivity(true);
    HttpServer server = vertx.createHttpServer(options);
    Router router = Router.router(vertx);

    // Browser entry point
    router.get("/").produces("text/html").handler(ctx -> {
      ctx.response()
          .end("<html><body><form action='/game' method='POST'>"
              + "<input type='submit' value='Join a game'/>"
              + "</form></body></html>");
    });

    // POST /game - richiedi di partecipare
    router.post("/game").produces("text/html").handler(ctx -> {
      String playerId = gameServer.create();
      ctx.response().setStatusCode(302)
          .putHeader("Location", "/game/" + playerId).end();
    });

    router.post("/game").produces("application/json").handler(ctx -> {
      String playerId = gameServer.create();
      ctx.response().putHeader("content-type", "application/json")
          .end("{\"game\":\"/game/" + playerId + "\"}\n");
    });

    // POST /game/{id} move=x - muovi
    router.route().handler(BodyHandler.create());
    router.post("/game/:playerId").produces("text/html").handler(ctx -> {
      String playerId = ctx.request().getParam("playerId");
      boolean open = gameServer.open(playerId);
      if (!open)
        ctx.response().setStatusCode(404)
            .end("<html><body><h1>Game not found.</h1></body></html>");

      Option<Tuple2<Integer, GameResult>> res = gameServer.status(playerId);
      if (res.isEmpty())
        ctx.response()
            .end("<html><body><h3>Please wait for another player.</h3>"
                + "<a href='/game/" + playerId + "'>Click here to refresh</a>"
                + "</body></html>");
      else {
        int idx = res.get()._1;
        GameResult status = res.get()._2;
        int move = Integer.valueOf(ctx.request().getParam("move"));
        if (idx == status.next && status.valid && !status.end)
          status = gameServer.player(playerId, move).getOrElse(status);

        ctx.response().end(render(playerId, idx, status));
      }
    });

    // GET /game/{id} - ottieni stato
    router.get("/game/:playerId").produces("text/html").handler(ctx -> {

      String playerId = ctx.request().getParam("playerId");
      boolean open = gameServer.open(playerId);

      if (!open)
        ctx.response().setStatusCode(404)
            .end("<html><body><h1>Game not found.</h1></body></html>");
      else {
        Option<Tuple2<Integer, GameResult>> res = gameServer.status(playerId);
        if (res.isEmpty())
          ctx.response()
              .end("<html><body><h3>Please wait for another player.</h3>"
                  + "<a href='/game/" + playerId + "'>Click here to refresh</a>"
                  + "</body></html>");
        else
          ctx.response().end(render(playerId, res.get()._1, res.get()._2));
      }
    });

    router.get("/game/:playerId").produces("application/json").handler(ctx -> {
      String playerId = ctx.request().getParam("playerId");
      boolean open = gameServer.open(playerId);

      if (!open)
        ctx.response().setStatusCode(404).end("{\"error\":\"Not found\"}\n");
      else {
        Option<Tuple2<Integer, GameResult>> res = gameServer.status(playerId);
        if (res.isEmpty())
          ctx.response().end("{\"gameStatus\":\"not started\"}\n");
        else
          ctx.response().end(renderJson(playerId, res.get()._1, res.get()._2));
      }
    });

    server.requestHandler(router::accept).listen(8080);

  }

  static String render(String gameId, int idx, GameResult game) {
    StringBuffer res = new StringBuffer("<html><body>");

    String playerName = idx == 0 ? "Player O" : "Player X";

    res = res.append("<h2>").append(playerName)
        .append("</h2><table border='1'>\n");
    String[] split = game.board.split("\\n");

    if (split.length == 4) {
      res = res.append("<tr><td>").append(split[0].replace(" ", "</td><td>"))
          .append("</td></tr>\n");
      res = res.append("<tr><td>").append(split[1].replace(" ", "</td><td>"))
          .append("</td></tr>\n");
      res = res.append("<tr><td>").append(split[2].replace(" ", "</td><td>"))
          .append("</td></tr>\n");
      res = res.append("\n</table>");
    }

    if (game.next == idx && !game.end && game.valid) {
      res.append("<form method='POST'>");
      for (String move : split[3].split("\\s")) {
        res.append("<input type='radio' name='move' value='").append(move)
            .append("' />").append(move).append("\n");
      }
      res.append("<br/><input type='submit' value='Move' /></form>");
    } else if (game.next == idx && game.end && game.valid) {
      res.append("You won!");
    } else if (game.next != idx && game.end && game.valid) {
      res.append("You lost");
    } else if (game.end && !game.valid) {
      res.append("The game is tied");
    } else {
      res.append("Please wait for the other player to move. "
          + "<a href='/game/" + gameId + "'>Click here to refresh</a>");
    }

    return res.append("</body></html>").toString();
  }

  static String renderJson(String gameId, int idx, GameResult game) {
    StringBuffer res = new StringBuffer("{ \"player\":\"");

    String playerName = idx == 0 ? "O" : "X";

    res = res.append(playerName).append("\",\n\"board\":[");
    String[] split = game.board.split("\\n");

    if (split.length == 4) {
      res = res.append("\"").append(split[0].replace(" ", "\",\""))
          .append("\",\"").append(split[1].replace(" ", "\",\""))
          .append("\",\"").append(split[2].replace(" ", "\",\"")).append("\"]");
    }

    if (game.next == idx && !game.end && game.valid) {
      res.append(",\n\"gameStatus\":\"waiting your move\",\n");
      res.append("\"availableMoves\":[");
      StringBuffer row = new StringBuffer();
      for (String s : split[3].split("\\s")) {
        row.append("\"" + s + "\",");
      }
      res.append(row.substring(0, row.length() - 1));
      res.append("]");
    } else if (game.next != idx && !game.end && game.valid) {
      res.append(",\n\"gameStatus\":\"waiting other move\"");
    } else if (game.next == idx && game.end && game.valid) {
      res.append(",\n\"gameStatus\":\"ended\",");
      res.append("\"gameResult\":\"won\"");
    } else if (game.next != idx && game.end && game.valid) {
      res.append(",\n\"gameStatus\":\"ended\",");
      res.append("\"gameResult\":\"lost\"");
    } else if (game.end && !game.valid) {
      res.append(",\n\"gameStatus\":\"ended\",");
      res.append("\"gameResult\":\"tied\"");
    }

    return res.append("}\n").toString();
  }
}
