package pcd2017.distribution.lesson8;

import static pcd2017.distribution.lesson8.Server2.PORT;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client2 {
  public static void main(String args[]) throws IOException {

    DatagramSocket socket = new DatagramSocket();
    byte[] buf = args[0].getBytes();
    InetAddress address = InetAddress.getByName("localhost");
    DatagramPacket packet = new DatagramPacket(buf, buf.length, address, PORT);
    socket.send(packet);
    socket.close();
  }
}
