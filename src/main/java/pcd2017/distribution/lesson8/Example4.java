package pcd2017.distribution.lesson8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Example4 {
  public static void main(String[] args)
      throws MalformedURLException, IOException {
    BufferedReader reader = new BufferedReader(
        new InputStreamReader(new URL("https://httpbin.org/get").openStream()));
    String line;
    while ((line = reader.readLine()) != null) {
      System.out.println(line);
    }
  }
}
