package pcd2017.distribution.lesson8;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

class EchoServer implements Runnable {

  private DatagramSocket socket;

  public EchoServer(int port) throws SocketException {
    socket = new DatagramSocket(port);
  }

  @Override
  public void run() {
    byte[] buf = new byte[256];
    DatagramPacket packet = new DatagramPacket(buf, buf.length);
    System.out.println("Server setup. Receiving...");
    try {
      socket.receive(packet);
      String received = new String(packet.getData(), 0, packet.getLength());
      System.out.println("Received: " + received);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      socket.close();
    }

  }

}

public class Server2 {

  public static int PORT = 58325;

  public static void main(String args[]) throws IOException {

    new Thread(new EchoServer(PORT)).run();

  }
}
