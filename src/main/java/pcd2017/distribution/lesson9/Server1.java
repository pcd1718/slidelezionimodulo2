package pcd2017.distribution.lesson9;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pcd2017.distribution.lesson8.Game;
import pcd2017.distribution.lesson8.GameResult;

class GameAttachment {
  final int id;
  final Game game;
  final AsynchronousServerSocketChannel server;
  final AsynchronousSocketChannel players[];
  final ByteBuffer readBuf = ByteBuffer.allocate(4);

  GameAttachment(int id, Game game, AsynchronousServerSocketChannel server) {
    this.id = id;
    this.game = game;
    this.server = server;
    this.players = null;
  }

  private GameAttachment(int id, Game game,
    AsynchronousServerSocketChannel server, AsynchronousSocketChannel playerO) {
    this.id = id;
    this.game = game;
    this.server = server;
    this.players = new AsynchronousSocketChannel[] { playerO };
  }

  private GameAttachment(int id, Game game,
    AsynchronousServerSocketChannel server, AsynchronousSocketChannel playerO,
    AsynchronousSocketChannel playerX) {
    this.id = id;
    this.game = game;
    this.server = server;
    this.players = new AsynchronousSocketChannel[] { playerO, playerX };
  }

  GameAttachment playerO(AsynchronousSocketChannel socket) {
    return new GameAttachment(id, game, server, socket);
  }

  GameAttachment playerX(AsynchronousSocketChannel socket) {
    return new GameAttachment(id, game, server, players[0], socket);
  }
}

/**
 * First step: accept player O
 * 
 * Next: accept player X
 *
 */
class AcceptPlayerO
    implements CompletionHandler<AsynchronousSocketChannel, GameAttachment> {

  @Override
  public void completed(AsynchronousSocketChannel result,
      GameAttachment attachment) {
    System.out.println(Thread.currentThread().getName() + " : game "
        + attachment.id + " connected player O");
    attachment.server.accept(attachment.playerO(result),
        new WriteFirstStatus());
  }

  @Override
  public void failed(Throwable exc, GameAttachment attachment) {
    exc.printStackTrace();
  }

}

/**
 * Write the first game status (no need to check for completed game)
 */
class WriteFirstStatus
    implements CompletionHandler<AsynchronousSocketChannel, GameAttachment> {
  @Override
  public void completed(AsynchronousSocketChannel result,
      GameAttachment attachment) {

    System.out.println(Thread.currentThread().getName() + " : game "
        + attachment.id + " connected for player X");
    attachment = attachment.playerX(result);

    GameResult status = attachment.game.status();
    AsynchronousSocketChannel socket = attachment.players[status.next];
    byte[] bytes = (status.toString() + "\n").getBytes();
    System.out.println(Thread.currentThread().getName() + " : game "
        + attachment.id + " started");
    socket.write(ByteBuffer.wrap(bytes), attachment, new ReadPlayer());

    // more games?
    if (attachment.id <= 5) {
      attachment.server.accept(
          new GameAttachment(attachment.id + 1, new Game(), attachment.server),
          new AcceptPlayerO());
    }
  }

  @Override
  public void failed(Throwable exc, GameAttachment attachment) {
    exc.printStackTrace();
  }
}

/**
 * Read move from player
 */
class ReadPlayer implements CompletionHandler<Integer, GameAttachment> {

  @Override
  public void completed(Integer result, GameAttachment attachment) {
    GameResult status = attachment.game.status();
    AsynchronousSocketChannel socket = attachment.players[status.next];
    System.out
        .println(Thread.currentThread().getName() + " : game " + attachment.id
            + " waiting input from player " + (status.next == 0 ? "O" : "X"));
    attachment.readBuf.clear();
    socket.read(attachment.readBuf, attachment, new WriteStatus());
  }

  @Override
  public void failed(Throwable exc, GameAttachment attachment) {
    exc.printStackTrace();
  }
}

/**
 * Check the game status and write to next player
 */
class WriteStatus implements CompletionHandler<Integer, GameAttachment> {
  @Override
  public void completed(Integer result, GameAttachment attachment) {

    String input = new String(attachment.readBuf.array(), 0, result).trim();
    // read received data
    Integer move = Integer.parseInt(input);
    GameResult initial = attachment.game.status();

    System.out.println(
        Thread.currentThread().getName() + " : game " + attachment.id + " read "
            + move + " from player " + (initial.next == 0 ? "O" : "X"));

    GameResult status = attachment.game.move(initial.next, move);

    if (!status.end) {
      // the game goes on
      AsynchronousSocketChannel socket = attachment.players[status.next];
      byte[] bytes = (status.toString() + "\n").getBytes();
      socket.write(ByteBuffer.wrap(bytes), attachment, new ReadPlayer());
    } else if (status.valid) {
      System.out
          .println(Thread.currentThread().getName() + " : game " + attachment.id
              + " ended with winner " + (status.next == 0 ? "O" : "X"));
      // we have a winner
      attachment.players[status.next].write(
          ByteBuffer.wrap("You won.".getBytes()), attachment,
          new CloseSocket(status.next));
      int loser = (status.next + 1) & 0x1;
      attachment.players[loser].write(ByteBuffer.wrap("You lost.".getBytes()),
          attachment, new CloseSocket(loser));
    } else {
      // we have a tie
      System.out.println(Thread.currentThread().getName() + " : game "
          + attachment.id + " tied");
      attachment.players[0].write(ByteBuffer.wrap("Tied.".getBytes()),
          attachment, new CloseSocket(0));
      attachment.players[1].write(ByteBuffer.wrap("Tied.".getBytes()),
          attachment, new CloseSocket(1));
    }
  }

  @Override
  public void failed(Throwable exc, GameAttachment attachment) {
    exc.printStackTrace();
  }
}

class CloseSocket implements CompletionHandler<Integer, GameAttachment> {

  private int idx;

  CloseSocket(int idx) {
    this.idx = idx;
  }

  @Override
  public void completed(Integer result, GameAttachment attachment) {
    try {
      attachment.players[idx].close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void failed(Throwable exc, GameAttachment attachment) {
    exc.printStackTrace();
  }

}

public class Server1 {

  public static final int GAME_PORT = 56763;

  public static void main(String[] args)
      throws IOException, InterruptedException {

    ExecutorService pool = Executors.newFixedThreadPool(4);
    AsynchronousChannelGroup group = AsynchronousChannelGroup
        .withThreadPool(pool);
    AsynchronousServerSocketChannel serverSocket = AsynchronousServerSocketChannel
        .open(group)
        .bind(new InetSocketAddress("127.0.0.1", Server1.GAME_PORT), 16);

    System.out.println("Accepting...");

    pool.submit(() -> {

      serverSocket.accept(new GameAttachment(1, new Game(), serverSocket),
          new AcceptPlayerO());
    });
  }
}
