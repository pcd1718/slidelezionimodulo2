package pcd2017.threads.lab1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Supplier;

import org.tukaani.xz.XZInputStream;

import pcd2017.threads.lab1.bowling.GameRecord;

/**
 * Legge un file di risultati, leggendo un record per riga.
 */
public class ScoreReader implements Supplier<GameRecord> {

  private BufferedReader reader;

  public ScoreReader(String fileName)
      throws FileNotFoundException, IOException {
    reader = new BufferedReader(new InputStreamReader(
        new XZInputStream(new FileInputStream(fileName))));
  }

  @Override
  public GameRecord get() {
    // FIXME
    return null;
  }

  static GameRecord parse(String line) {
    // FIXME
    return null;
  }

}
