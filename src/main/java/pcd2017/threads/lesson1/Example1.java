package pcd2017.threads.lesson1;

/**
 * Starts a Thread
 */
public class Example1 {

  public static void main(String[] args) {
    Thread a = new ThreadBuilder().get();

    System.out.println("Starting Threads");
    a.start();
    System.out.println("Done starting.");
  }

}
