package pcd2017.threads.lesson1;

import java.util.stream.Stream;

public class Example2 {

  /**
   * The output is different for every run
   * 
   * @param args
   */
  public static void main(String[] args) {
    Stream<Thread> threads = Stream.generate(new ThreadBuilder());

    System.out.println("Starting Threads");
    threads.limit(10).forEach((Thread a) -> a.start());
    System.out.println("Done starting.");
  }

}
