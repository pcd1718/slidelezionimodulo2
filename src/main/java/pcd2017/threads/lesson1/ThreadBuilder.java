package pcd2017.threads.lesson1;

import java.util.function.Supplier;

/**
 * Supplies a Thread that waits a random time
 *
 */
public class ThreadBuilder implements Supplier<Thread> {

  Supplier<Long> waitTime;

  public ThreadBuilder() {
    waitTime = () -> Math.round(Math.random() * 1000);
  }

  public ThreadBuilder(long wait) {
    waitTime = () -> wait;
  }

  @Override
  public Thread get() {
    return new Thread(() -> {
      String s = Thread.currentThread().getName();
      long t = waitTime.get();
      System.out.println(s + " will wait for " + t + " ms.");
      try {
        Thread.sleep(t);
        System.out.println(s + " is done wating for " + t + " ms.");
      } catch (InterruptedException e) {
        System.out.println(s + " has been interrupted!");
        e.printStackTrace();
      }
    });
  }

}