package pcd2017.threads.lesson2;

import pcd2017.threads.lesson1.ThreadBuilder;

public class Example1 {

  public static void main(String[] args) {

    final Thread target = new ThreadBuilder(800L).get();
    final Thread observer = new Thread(() -> {
      System.out.println("Target Thread is live: " + target.isAlive());
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(100L);
          System.out.println("Target Thread is live: " + target.isAlive());
        } catch (InterruptedException e) {
          System.out.println("Observer Interrupted");
          e.printStackTrace();
        }
      }
      System.out.println("Target Thread is live: " + target.isAlive());
    });

    observer.start();
    target.start();

  }

}
