package pcd2017.threads.lesson2;

import java.util.stream.Stream;

import pcd2017.threads.lesson1.ThreadBuilder;

public class Example2 {

  /**
   * Thread#run runs in the same thread as the caller
   * 
   * @param args
   */
  public static void main(String[] args) {
    Stream<Thread> threads = Stream.generate(new ThreadBuilder());

    System.out.println("Starting Threads");
    threads.limit(10).forEach((Thread a) -> a.run());
    System.out.println("Done starting.");
  }

}
