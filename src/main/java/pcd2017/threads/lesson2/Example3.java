package pcd2017.threads.lesson2;

import pcd2017.threads.lesson1.ThreadBuilder;

public class Example3 {

  public static void main(String[] args) {

    final Thread target = new ThreadBuilder(2000L).get();
    final Thread observer = new Thread(() -> {
      System.out.println("Target Thread is live: " + target.isAlive());
      try {
        Thread.sleep(1000L);
        System.out.println("Interrupting target thread");
        target.interrupt();
        System.out.println("Target interrupted.");
      } catch (InterruptedException e) {
        System.out.println("Observer Interrupted");
        e.printStackTrace();
      }

      System.out.println("Target Thread is live: " + target.isAlive());
    });

    observer.start();
    target.start();

  }

}
