package pcd2017.threads.lesson2;

import java.util.function.Supplier;

class RethrowingThreadBuilder implements Supplier<Thread> {

  Supplier<Long> waitTime;

  public RethrowingThreadBuilder(long wait) {
    waitTime = () -> wait;
  }

  @Override
  public Thread get() {
    return new Thread(() -> {
      String s = Thread.currentThread().getName();
      long t = waitTime.get();
      System.out.println(s + " will wait for " + t + " ms.");
      try {
        Thread.sleep(t);
        System.out.println(s + " is done wating for " + t + " ms.");
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    });
  }

}

public class Example4 {

  public static void main(String[] args) {

    final Thread target = new RethrowingThreadBuilder(2000L).get();

    target.setUncaughtExceptionHandler((Thread t, Throwable e) -> {
      System.out.println("Thread " + t.getName() + " has thrown:\n" + e.getClass() + ": " + e.getMessage());
    });

    final Thread observer = new Thread(() -> {
      System.out.println("Target Thread is live: " + target.isAlive());
      try {
        Thread.sleep(1000L);
        System.out.println("Interrupting observed thread");
        target.interrupt();
        System.out.println("Target interrupted.");
      } catch (InterruptedException e) {
        System.out.println("Observer Interrupted");
        e.printStackTrace();
      }

      System.out.println("Target Thread is live: " + target.isAlive());
    });

    observer.start();
    target.start();

  }

}
