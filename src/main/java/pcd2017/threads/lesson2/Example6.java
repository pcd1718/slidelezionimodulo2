package pcd2017.threads.lesson2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class Example6 {

  public static void main(String[] args) {

    ExecutorService service = Executors.newSingleThreadExecutor();

    Stream<Runnable> threads = Stream.generate(new RunnableBuilder());
    System.out.println("Scheduling runnables");
    threads.limit(10).forEach((Runnable r) -> service.execute(r));
    System.out.println("Done scheduling.");
  }

}
