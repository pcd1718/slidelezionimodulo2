package pcd2017.threads.lesson2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Example8 {

  public static void main(String[] args) throws InterruptedException, ExecutionException {

    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
    Supplier<Callable<Integer>> supplier = new FactorialBuilder();
    List<Callable<Integer>> callables = new ArrayList<Callable<Integer>>();
    for (int i = 0; i < 10; i++)
      callables.add(supplier.get());

    System.out.println("Scheduling computations");
    List<Future<Integer>> futures = executor.invokeAll(callables);
    System.out.println("Done scheduling.");

    while (executor.getCompletedTaskCount() < callables.size()) {
      System.out.printf("Completed Tasks: %d: %s\n", executor.getCompletedTaskCount(), format(futures));
      TimeUnit.MILLISECONDS.sleep(50);
    }

    System.out.printf("Completed Tasks: %d: %s\n", executor.getCompletedTaskCount(), format(futures));

  }

  private static String format(List<Future<Integer>> futures) throws InterruptedException, ExecutionException {
    StringBuilder done = new StringBuilder();
    for (Future<Integer> future : futures) {
      if (future.isDone())
        done.append(String.format("%6d", future.get()));
      else
        done.append(" _____");
    }
    return done.toString();
  }

}
