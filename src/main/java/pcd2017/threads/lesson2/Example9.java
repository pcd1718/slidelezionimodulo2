package pcd2017.threads.lesson2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Supplier;

public class Example9 {

  public static void main(String[] args) throws InterruptedException, ExecutionException {

    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
    Supplier<Callable<Integer>> supplier = new FactorialBuilder();
    List<Callable<Integer>> callables = new ArrayList<Callable<Integer>>();
    for (int i = 0; i < 10; i++)
      callables.add(supplier.get());

    System.out.println("Scheduling computations");
    Integer result = executor.invokeAny(callables);
    System.out.println("Done invoking: " + result);

    executor.shutdown();
  }

}
