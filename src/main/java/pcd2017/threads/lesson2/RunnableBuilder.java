package pcd2017.threads.lesson2;

import java.util.function.Supplier;

/**
 * Supplies a Runnable that waits a random time
 *
 */
public class RunnableBuilder implements Supplier<Runnable> {

  Supplier<Long> waitTime;
  int counter = 0;

  public RunnableBuilder() {
    waitTime = () -> Math.round(Math.random() * 1000);
  }

  public RunnableBuilder(long wait) {
    waitTime = () -> wait;
  }

  @Override
  public Runnable get() {
    final int id = counter++;
    return () -> {
      String s = Thread.currentThread().getName();
      long t = waitTime.get();
      System.out.println(s + " (" + id + ") will wait for " + t + " ms.");
      try {
        Thread.sleep(t);
        System.out.println(s + " (" + id + ") is done wating for " + t + " ms.");
      } catch (InterruptedException e) {
        System.out.println(s + " has been interrupted!");
        e.printStackTrace();
      }
    };
  }

}