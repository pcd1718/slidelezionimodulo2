package pcd2017.threads.lesson3;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

interface Counter {

  /**
   * Increment the state
   */
  public void add();

  /**
   * Read the internal value of the Counter
   * 
   * @return the actual value
   */
  public int getState();
}

class UnsyncCounter implements Counter {
  private int state = 0;

  public void add() {
    int current = state;
    System.out.println("current " + current);
    try {
      TimeUnit.MILLISECONDS.sleep(Math.round(Math.random() * 100));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    state = current + 1;
    System.out.println("added up to " + state);
  }

  public int getState() {
    return state;
  }
}

class Incrementer implements Callable<Boolean> {

  private Counter counter;

  Incrementer(Counter counter) {
    this.counter = counter;
  }

  @Override
  public Boolean call() {
    IntStream.range(0, 10).forEach((i) -> counter.add());
    return true;
  }

}

/**
 * Unsyncronized, concurrent access to the same state.
 */
public class Example1 {
  public static void main(String[] args) throws InterruptedException, ExecutionException {

    ExecutorService executor = Executors.newFixedThreadPool(1);
    // ExecutorService executor = Executors.newFixedThreadPool(4);
    // ExecutorService executor = Executors.newSingleThreadExecutor();

    Counter counter = new UnsyncCounter();
    List<Incrementer> incs = Arrays.asList(new Incrementer(counter), new Incrementer(counter), new Incrementer(counter),
        new Incrementer(counter));
    long time = System.currentTimeMillis();
    System.out.println("Running...");
    executor.invokeAll(incs);
    long end = System.currentTimeMillis();
    System.out.println("All done. Final state: " + counter.getState() + " (" + (end - time) + ")");
  }
}
