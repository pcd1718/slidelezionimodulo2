package pcd2017.threads.lesson3;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class SyncCounter implements Counter {
  private int state = 0;

  synchronized public void add() {
    int current = state;
    System.out.println("current " + current);
    try {
      TimeUnit.MILLISECONDS.sleep(Math.round(Math.random() * 100));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    state = current + 1;
    System.out.println("added up to " + state);
  }

  public int getState() {
    return state;
  }
}

public class Example2 {
  public static void main(String[] args) throws InterruptedException, ExecutionException {

    ExecutorService executor = Executors.newFixedThreadPool(1);
    // ExecutorService executor = Executors.newFixedThreadPool(4);
    // ExecutorService executor = Executors.newSingleThreadExecutor();

    Counter counter = new SyncCounter();
    List<Incrementer> incs = Arrays.asList(new Incrementer(counter), new Incrementer(counter), new Incrementer(counter),
        new Incrementer(counter));
    long time = System.currentTimeMillis();
    System.out.println("Running...");
    executor.invokeAll(incs);
    long end = System.currentTimeMillis();
    System.out.println("All done. Final state: " + counter.getState() + " (" + (end - time) + ")");
  }
}
