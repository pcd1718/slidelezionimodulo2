package pcd2017.threads.lesson3;

class Named {
  public final String name;
  private boolean red = false;

  Named(String name) {
    this.name = name;
  }

  synchronized void perform() throws InterruptedException {
    if (!red) {
      red = true;
      this.wait();
    } else {
      red = false;
      this.notify();
    }
  }

  synchronized void performAll() throws InterruptedException {
    if (!red) {
      red = true;
      this.wait();
    } else {
      red = false;
      this.notifyAll();
    }
  }

}

class Waiter implements Runnable {
  private final Named first, second;

  Waiter(Named first, Named second) {
    this.first = first;
    this.second = second;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " waiting on " + first.name);
    try {
      first.perform();
    } catch (InterruptedException e) {
      System.out.println(Thread.currentThread().getName() + " interrupted on " + first.name);
    }
    System.out.println(Thread.currentThread().getName() + " signalled on " + first.name);
    System.out.println(Thread.currentThread().getName() + " waiting on " + second.name);
    try {
      second.perform();
    } catch (InterruptedException e) {
      System.out.println(Thread.currentThread().getName() + " interrupted on " + second.name);

    }
    System.out.println(Thread.currentThread().getName() + " signalled on " + second.name);
  }

}

public class Example4 {

  public static void main(String[] args) {
    Named a = new Named("a"), b = new Named("b"), c = new Named("c"), d = new Named("d");

    new Thread(new Waiter(a, d)).start();
    new Thread(new Waiter(b, d)).start();
    new Thread(new Waiter(c, d)).start();

    try {
      a.perform();
      b.perform();
      c.perform();
      d.performAll();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }
}
