package pcd2017.threads.lesson3;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

/**
 * Bounded random character source
 */
class CharSource {
  private final String[] content;
  private int index = 0;

  public CharSource(int lines, int length) {
    Random random = new Random();
    content = new String[lines];
    IntStream.range(0, lines).forEach((i) -> {
      StringBuffer target = new StringBuffer(length);
      IntStream.range(0, random.nextInt(length) + 1).forEach((j) -> target.append('0' + random.nextInt('z' - '0')));
      content[i] = target.toString();
    });
  }

  public boolean hasMoreLines() {
    return index < content.length;
  }

  public Optional<String> getLine() {
    if (this.hasMoreLines()) {
      System.out.println("Source: " + (content.length - index));
      return Optional.of(content[index++]);
    }
    return Optional.empty();
  }
}

/**
 * Thread-safe buffer using Locks and Conditions
 */
class Buffer {
  private LinkedList<String> buffer;
  private int maxSize;
  private ReentrantLock lock;
  private Condition lines;
  private Condition space;
  private boolean pendingLines;

  public Buffer(int maxSize) {
    this.maxSize = maxSize;
    buffer = new LinkedList<>();
    lock = new ReentrantLock();
    lines = lock.newCondition();
    space = lock.newCondition();
    pendingLines = true;
  }

  public void insert(String line) {
    lock.lock();
    try {
      while (buffer.size() == maxSize) {
        space.await();
      }
      buffer.offer(line);
      System.out.printf("%s: Inserted Line: %d\n", Thread.currentThread().getName(), buffer.size());
      lines.signalAll();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  public Optional<String> get() {
    Optional<String> line = Optional.empty();
    lock.lock();
    try {
      while ((buffer.size() == 0) && (hasPendingLines())) {
        lines.await();
      }
      if (hasPendingLines()) {
        line = Optional.ofNullable(buffer.poll());
        System.out.printf("%s: Line Read: %d\n", Thread.currentThread().getName(), buffer.size());
        space.signalAll();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
    return line;
  }

  public boolean hasPendingLines() {
    return pendingLines || buffer.size() > 0;
  }

  public void setPendingLines(boolean pendingLines) {
    this.pendingLines = pendingLines;
  }

}

/**
 * Reads from the source and inserts in the buffer
 */
class Producer implements Runnable {
  private CharSource source;
  private Buffer buffer;

  public Producer(CharSource source, Buffer buffer) {
    this.source = source;
    this.buffer = buffer;
  }

  @Override
  public void run() {
    buffer.setPendingLines(true);
    while (source.hasMoreLines())
      source.getLine().ifPresent((line) -> {
        buffer.insert(line);
        Example7.randomWait(50);
      });
    buffer.setPendingLines(false);
  }
}

/**
 * Reads a line from the buffer and ponders on it.
 */
class Consumer implements Runnable {
  private Buffer buffer;

  public Consumer(Buffer buffer) {
    this.buffer = buffer;
  }

  @Override
  public void run() {
    while (buffer.hasPendingLines())
      buffer.get().ifPresent((line) -> process(line));
  }

  private void process(String line) {
    Example7.randomWait(250);
  }

}

public class Example7 {
  public static void main(String[] args) {
    CharSource source = new CharSource(100, 100);
    Buffer buffer = new Buffer(20);

    Thread producer = new Thread(new Producer(source, buffer), "producer");

    Thread[] consumers = new Thread[] { new Thread(new Consumer(buffer)), new Thread(new Consumer(buffer)),
        new Thread(new Consumer(buffer)) };

    producer.start();
    for (Thread t : consumers)
      t.start();
  }

  static void randomWait(int max) {
    try {
      Random random = new Random();
      Thread.sleep(random.nextInt(max));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
