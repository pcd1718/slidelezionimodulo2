package pcd2017.threads.lesson3;

public interface Printer {
  public void printJob(Object document);
}