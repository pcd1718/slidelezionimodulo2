package pcd2017.threads.lesson4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

class Holder {
  public int counter = 0;
}

public class Example1 {

  public static void main(String[] args) {
    Holder holder = new Holder();

    ExecutorService executor = Executors.newFixedThreadPool(4);

    IntStream.range(0, 10000).forEach(i -> executor.submit(() -> holder.counter++));

    awaitDone(executor);

    System.out.println(holder.counter);
  }

  static void awaitDone(ExecutorService executor) {
    executor.shutdown();
    while (!executor.isTerminated())
      try {
        System.out.print(".");
        executor.awaitTermination(1, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
  }
}
