package pcd2017.threads.lesson4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Example2 {

  public static void main(String[] args) {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");
    list.add("c");

    new Thread(() -> {
      list.iterator().forEachRemaining(el -> {
        System.out.println(el);
        sleep(250);
      });
    }).start();
    ;

    new Thread(() -> {
      sleep(300);
      list.add("d");
    }).start();

  }

  public static void sleep(int millis) {
    try {
      TimeUnit.MILLISECONDS.sleep(millis);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
