package pcd2017.threads.lesson4;

import static pcd2017.threads.lesson4.Example1.awaitDone;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class Example3 {

  public static void main(String[] args) {
    AtomicInteger counter = new AtomicInteger(0);

    ExecutorService executor = Executors.newFixedThreadPool(4);

    IntStream.range(0, 10000).forEach(i -> executor.submit(() -> counter.incrementAndGet()));

    awaitDone(executor);

    System.out.println(counter.get());

  }
}
