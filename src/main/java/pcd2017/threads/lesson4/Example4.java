package pcd2017.threads.lesson4;

import static pcd2017.threads.lesson4.Example1.awaitDone;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

class VolatileHolder {
  volatile int counter = 0;
}

public class Example4 {

  public static void main(String[] args) {
    VolatileHolder holder = new VolatileHolder();

    ExecutorService executor = Executors.newFixedThreadPool(4);

    IntStream.range(0, 10000).forEach(i -> executor.submit(() -> holder.counter++));

    awaitDone(executor);

    System.out.println(holder.counter);

  }
}
