package pcd2017.threads.lesson4;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

public class Example5 {

  public static void main(String[] args) {

    Random rnd = new Random();
    ConcurrentHashMap<String, Long> map = new ConcurrentHashMap<String, Long>();

    IntStream.range(0, 10000)
        .forEach(i -> map.put("k" + i, new Long(rnd.nextInt(1000))));
    long start = System.currentTimeMillis();
    Long parres = map.reduceEntries(500, entry -> entry.getValue(),
        (a, b) -> a + b);
    long partime = System.currentTimeMillis() - start;

    start = System.currentTimeMillis();
    Long serres = map.reduceEntries(10000001, entry -> entry.getValue(),
        (a, b) -> a + b);
    long sertime = System.currentTimeMillis() - start;

    System.out.println("Parallel sum:" + parres + " in " + partime
        + "\nSerial sum:" + serres + " in " + sertime);
  }
}
