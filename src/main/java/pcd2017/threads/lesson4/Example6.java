package pcd2017.threads.lesson4;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.IntStream;

import pcd2017.threads.lesson3.Printer;

class PrintJob {
  final Date enqueued;
  final Object document;

  PrintJob(Object document) {
    this.enqueued = new Date();
    this.document = document;
  }
}

class PrintDriver implements Runnable {

  private final BlockingQueue<PrintJob> queue;
  private final Random rnd;

  PrintDriver(BlockingQueue<PrintJob> queue) {
    this.queue = queue;
    rnd = new Random();
  }

  @Override
  public void run() {
    try {
      while (true) {
        PrintJob job = queue.take();
        System.out.printf("%s: Took a document that waited %d\n",
            Thread.currentThread().getName(),
            System.currentTimeMillis() - job.enqueued.getTime());
        int duration = rnd.nextInt(2500);
        // Attenzione: usare TimeUnit.X.wait genera un'eccezione: quale e perché?
        Thread.sleep(duration);
        System.out.printf("%s: Printed a document in %d\n",
            Thread.currentThread().getName(), duration);
      }
    } catch (InterruptedException ex) {
      System.out.println("Printer shutting down.");
    }

  }

}

class ConcurrentPrinter implements Printer {

  private int size;
  private LinkedBlockingQueue<PrintJob> queue;
  private ExecutorService executor;

  ConcurrentPrinter(int printers) {
    size = printers < 4 ? printers : 4;
    queue = new LinkedBlockingQueue<PrintJob>(10);
    executor = Executors.newFixedThreadPool(size);
    IntStream.range(0, size)
        .forEach((a) -> executor.execute(new PrintDriver(queue)));
  }

  @Override
  public void printJob(Object document) {
    try {
      queue.put(new PrintJob(document));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  void shutdown() {
    executor.shutdownNow();
  }

}

class Job implements Runnable {
  private Printer printQueue;

  public Job(Printer printQueue) {
    this.printQueue = printQueue;
  }

  @Override
  public void run() {
    System.out.printf("%s: Going to print a document\n",
        Thread.currentThread().getName());
    printQueue.printJob(new Object());
    System.out.printf("%s: The document has been enqueued\n",
        Thread.currentThread().getName());
  }

}

public class Example6 {

  public static void main(String[] args) {
    Printer concurrent = new ConcurrentPrinter(2);
    Thread thread[] = new Thread[10];
    for (int i = 0; i < 10; i++) {
      thread[i] = new Thread(new Job(concurrent), "Job " + i);
    }
    for (int i = 0; i < 10; i++) {
      thread[i].start();
    }
  }
}
