package pcd2017.threads.lesson4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

class LocalVar {

  // Atomic integer containing the next thread ID to be assigned
  private static final AtomicInteger nextId = new AtomicInteger(0);

  ThreadLocal<Integer> counter;

  LocalVar() {
    counter = new ThreadLocal<Integer>() {
      @Override
      protected Integer initialValue() {
        return nextId.incrementAndGet();
      }
    };
  }

  Integer get() {
    return counter.get();
  }

}

class LocalReader implements Runnable {

  private LocalVar var;
  private int item;

  LocalReader(LocalVar var, int item) {
    this.var = var;
    this.item = item;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + ", item " + item
        + ": read " + var.get());
  }

}

public class Example7 {

  public static void main(String[] args) {

    ExecutorService executor = Executors.newFixedThreadPool(4);
    LocalVar var = new LocalVar();
    IntStream.range(0, 20)
        .forEach((a) -> executor.execute(new LocalReader(var, a)));
    executor.shutdown();
  }

}
