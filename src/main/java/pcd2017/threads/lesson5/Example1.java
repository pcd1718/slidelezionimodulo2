package pcd2017.threads.lesson5;

import java.util.stream.IntStream;

public class Example1 {

  public static void main(String[] args) {
    long start = System.currentTimeMillis();
    IntStream.range(0, 1000000).boxed().parallel() // esamina i primi n interi
        .map(new Divisors()) // trova divisori
        .filter(new Perfect()) // solo i numeri perfetti
        .forEach((i) -> System.out.println(i.toString()));
    long end = System.currentTimeMillis();
    System.out.println(end - start);

  }

}
