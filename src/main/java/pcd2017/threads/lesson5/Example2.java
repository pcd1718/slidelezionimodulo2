package pcd2017.threads.lesson5;

import java.util.stream.IntStream;

public class Example2 {

  public static void main(String[] args) {
    long cnt = IntStream.range(1, 20).map((i) -> {
      System.out.println(i);
      return i * 2;
    }).limit(5).count();
    System.out.println(cnt);

  }

}
