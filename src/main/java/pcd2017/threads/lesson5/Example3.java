package pcd2017.threads.lesson5;

import java.util.stream.IntStream;

public class Example3 {

  public static void main(String[] args) {
    IntStream.range(1, 20).parallel().map((i) -> {
      System.out.println(i + " " + Thread.currentThread().getName());
      return i * 2;
    }).forEachOrdered((i) -> {
      System.out.println(">>> " + i);
    });

  }

}
