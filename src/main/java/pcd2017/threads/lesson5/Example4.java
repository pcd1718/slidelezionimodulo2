package pcd2017.threads.lesson5;

import java.util.Optional;
import java.util.stream.IntStream;

public class Example4 {

  public static void main(String[] args) {
    Optional<CandidateNumber> match = IntStream.range(30, 1000).boxed()
        .parallel().map(new Divisors()).map((CandidateNumber c) -> {
          System.out.println(c.toString());
          return c;
        }).filter(new Perfect()).findAny();
    System.out.println(">>> " + match.get().toString());

  }

}
