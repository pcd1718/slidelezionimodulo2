package pcd2017.threads.lesson5;

import java.util.stream.IntStream;

public class Example5 {

  public static void main(String[] args) {
    IntStream.range(10, 10000).boxed().parallel().map(new Divisors())
        .map((CandidateNumber c) -> {
          System.out.println(c.toString());
          return c;
        }).filter(new Perfect()).limit(2).forEach((CandidateNumber c) -> {
          System.out.println(">>> " + c.toString());
        });
  }

}
