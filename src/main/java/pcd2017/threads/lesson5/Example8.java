package pcd2017.threads.lesson5;

import java.util.stream.IntStream;

public class Example8 {

  public static void main(String[] args) {
    String res = IntStream.range(1, 1001).boxed().map((i) -> i.toString())
        .parallel().reduce("", String::concat);
    System.out.println(">>>> " + res);
  }

}
