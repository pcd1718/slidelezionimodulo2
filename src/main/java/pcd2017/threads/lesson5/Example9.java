package pcd2017.threads.lesson5;

import java.util.stream.IntStream;

public class Example9 {

  public static void main(String[] args) {
    String res = IntStream.range(1, 1001).boxed().map((i) -> i.toString())
        .parallel()
        .collect(() -> new StringBuffer(), // supplier
            (acc, el) -> acc.append(el), // accumulator
            (resA, resB) -> resA.append(resB)) // combiner
        .toString();
    System.out.println(">>>> " + res);
  }

}
