package pcd2017.threads.lesson5;

import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Perfect implements Predicate<CandidateNumber> {

  @Override
  public boolean test(CandidateNumber c) {
    Integer sum = c.divisors.stream()
        .collect(Collectors.summingInt((Integer n) -> n));
    return (sum + 1) == c.n;
  }

}