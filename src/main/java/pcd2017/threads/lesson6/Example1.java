package pcd2017.threads.lesson6;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import pcd2017.threads.lesson5.CandidateNumber;
import pcd2017.threads.lesson5.Divisors;
import pcd2017.threads.lesson5.Perfect;

class RxDivisors implements Function<Integer, CandidateNumber> {

  private final Divisors delegate = new Divisors();

  @Override
  public CandidateNumber apply(Integer i) throws Exception {
    return delegate.apply(i);
  }

}

class RxPerfect implements Predicate<CandidateNumber> {
  private final Perfect delegate = new Perfect();

  @Override
  public boolean test(CandidateNumber i) throws Exception {
    boolean result = delegate.test(i);
    if (result)
      System.out.println("  testing: " + Thread.currentThread().getName());
    return result;
  }

}

public class Example1 {
  static public void main(String[] args) {
    System.out.println("Defining...");
    Observable.range(0, 10000).map(new RxDivisors()).filter(new RxPerfect())
        .subscribe((c) -> {
          System.out.println(c);
        }, (t) -> {
          t.printStackTrace();
        }, () -> {
          System.out.println("Done");
        });
    System.out.println("Defined");
  }
}
