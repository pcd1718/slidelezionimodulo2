package pcd2017.distribution.lesson8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GameResultTest {

  @Test
  public void testBlank() {
    GameResult gameResult = new GameResult(Game.PLAYER_O, true, false,
        new int[] { 0, 0 });
    String repr = gameResult.toString();
    System.out.println(repr);
    assertEquals("Inizio partita",
        "PLAYER_O true false\n_ _ _\n_ _ _\n_ _ _\n0 1 2 3 4 5 6 7 8", repr);

  }

  @Test
  public void testOne() {
    GameResult gameResult = new GameResult(Game.PLAYER_X, true, false,
        new int[] { 0x4, 0 });
    String repr = gameResult.toString();
    System.out.println(repr);
    assertEquals("Inizio partita",
        "PLAYER_X true false\n_ _ O\n_ _ _\n_ _ _\n0 1 3 4 5 6 7 8", repr);
  }

  @Test
  public void testFinal() {
    GameResult gameResult = new GameResult(Game.PLAYER_X, true, true,
        new int[] { 0xc5, 0x138 });
    String repr = gameResult.toString();
    System.out.println(repr);
    assertEquals("Fine partita", "PLAYER_X true true\nO _ O\nX X X\nO O X\n1",
        repr);
  }

}
