package pcd2017.threads.lab1;

import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Test;

import pcd2017.threads.lab1.data.DataRecord;

public class SummarizerTest {

  @Test
  public void test() throws InterruptedException {
    BlockingQueue<DataRecord> queue = new LinkedBlockingQueue<DataRecord>(15);
    queue.put(new DataRecord("Jeffrey Leboski", 190, 5, 4, 0));
    queue.put(new DataRecord("Jeffrey Leboski", 210, 6, 5, 0));
    queue.put(new DataRecord("Walter Sobchak", 130, 2, 4, 1));

    Summarizer runnable = new Summarizer(queue);
    runnable.done();
    runnable.run();
    Map<String, int[]> results = runnable.results();

    assertEquals("Size", 2, results.size());

    int[] leboski = new int[] { 2, 400, 11, 9, 0, 190, 210 };
    int[] sobchak = new int[] { 1, 130, 2, 4, 1, 130, 130 };
    for (int idx = 0; idx < leboski.length; idx++) {
      assertEquals("Leboski " + idx, leboski[idx],
          results.get("Jeffrey Leboski")[idx]);
      assertEquals("Sobchak " + idx, sobchak[idx],
          results.get("Walter Sobchak")[idx]);
    }

  }

}
