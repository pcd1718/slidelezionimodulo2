package pcd2017.threads.lab1.bowling;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pcd2017.threads.lab1.bowling.BowlingGame;

@RunWith(Parameterized.class)
public class FinalFrameTest {

  @Parameters
  public static List<StringVal> finalFrame() {
    return Arrays.asList(new StringVal("1/35XXX458/X3/23", 160, 4, 3),
        new StringVal("1/35XXX458/X3/XX6", 189, 6, 3),
        new StringVal("1/35XXX458/X35", 149, 4, 2),
        new StringVal("1/35XXX458/X3/X--", 173, 5, 3),
        new StringVal("XXXXXXXXXXXX", 300, 12, 0),
        new StringVal("XXXXXXXXXX12", 274, 10, 0),
        new StringVal("1/35XXX458/X3/--", 153, 4, 3),
        new StringVal("5/5/5/5/5/5/5/5/5/5/5", 150, 0, 10),
        new StringVal("9-8/--9-9-9-9-9-9-9/1", 84, 0, 2),
        new StringVal("9-X8-9-9-9-9-9-9-X23", 104, 2, 0));
  }

  private final StringVal input;

  public FinalFrameTest(StringVal input) {
    this.input = input;
  }

  @Test
  public void test() {
    BowlingGame res = new BowlingGame(input.value);
    assertEquals(input.value, input.score, res.score());
    assertEquals(input.value, input.spares, res.spares());
    assertEquals(input.value, input.strikes, res.strikes());
  }
}
