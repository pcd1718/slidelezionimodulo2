package pcd2017.threads.lab1.bowling;

import io.vavr.collection.List;

class IntListVal {

  public final List<Integer> vals;
  public final int expected;

  IntListVal(int expected, Integer... vals) {
    this.expected = expected;
    this.vals = List.of(vals);
  }

}
