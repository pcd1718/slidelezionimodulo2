package pcd2017.threads.lab1.bowling;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class NormalPlay {

  @Parameters
  public static List<StringVal> normalPlays() {
    return Arrays.asList(new StringVal("1/35XXX45", 103, 3, 1),
        new StringVal("11111111112222222222", 30, 0, 0),
        new StringVal("--------------------", 0, 0, 0),
        new StringVal("1-1----------------1", 3, 0, 0),
        new StringVal("9-9-9-9-9-9-9-9-9-9-", 90, 0, 0),
        new StringVal("5/11------------3/11", 26, 0, 2),
        new StringVal("9-8/--9-9-9-9-9-9-9-", 82, 0, 1),
        new StringVal("--8/1---------------", 12, 0, 1),
        new StringVal("--8/-1--------------", 11, 0, 1),
        new StringVal("9-X8-9-9-9-9-9-9-9-", 98, 1, 0),
        new StringVal("--X81--------------", 28, 1, 0),
        new StringVal("--X8-1-------------", 27, 1, 0),
        new StringVal("8/9/72XXX3/819/XX5", 190, 5, 4),
        new StringVal("X3/4/6-X-1-/-/X54", 126, 3, 4));
  }

  private final StringVal input;

  public NormalPlay(StringVal input) {
    this.input = input;
  }

  @Test
  public void test() {
    BowlingGame res = new BowlingGame(input.value);
    assertEquals(input.value, input.score, res.score());
    assertEquals(input.value, input.spares, res.spares());
    assertEquals(input.value, input.strikes, res.strikes());
  }
}
