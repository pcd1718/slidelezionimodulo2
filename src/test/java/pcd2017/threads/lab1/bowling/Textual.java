package pcd2017.threads.lab1.bowling;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pcd2017.threads.lab1.bowling.BowlingGame;

@RunWith(Parameterized.class)
public class Textual {

  @Parameters
  public static List<StringVal> textual() {
    return Arrays.asList(new StringVal("--", 0, 0, 0),
        new StringVal("1-", 1, 0, 0), new StringVal("13", 4, 0, 0),
        new StringVal("13521-", 12, 0, 0), new StringVal("1-5-", 6, 0, 0),
        new StringVal("1/--", 10, 0, 1), new StringVal("1/--", 10, 0, 1),
        new StringVal("1/-5", 15, 0, 1), new StringVal("1/35--", 21, 0, 1),
        new StringVal("1/3/23", 30, 0, 2), new StringVal("X--", 10, 1, 0),
        new StringVal("X--51", 16, 1, 0), new StringVal("X51", 22, 1, 0));
  }

  private final StringVal input;

  public Textual(StringVal input) {
    this.input = input;
  }

  @Test
  public void test() {
    BowlingGame res = new BowlingGame(input.value);
    assertEquals(input.value, input.score, res.score());
    assertEquals(input.value, input.spares, res.spares());
    assertEquals(input.value, input.strikes, res.strikes());
  }
}
