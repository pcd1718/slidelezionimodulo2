package pcd2017.threads.lab1.solution;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pcd2017.threads.lab1.bowling.GameRecord;
import pcd2017.threads.lab1.data.DataRecord;

public class GameRecordToDataTest {

  @Test
  public void test() {
    GameRecordToData gameRecordToPlayer = new GameRecordToData(
        GameRecord::player);
    GameRecordToData gameRecordToVenueLane = new GameRecordToData(
        (GameRecord r) -> {
          return GameRecord.venue(r) + "-" + GameRecord.lane(r);
        });
    GameRecord record = new GameRecord("Jeffrey Leboski",
        "Hollywood Star Lanes", 4, "8/9/72XXX3/819/XX5");

    DataRecord player = gameRecordToPlayer.apply(record);
    DataRecord venueLane = gameRecordToVenueLane.apply(record);

    assertEquals("Player", "Jeffrey Leboski", player.key);
    assertEquals("Player score", 190, player.score);
    assertEquals("Venue Lane", "Hollywood Star Lanes-4", venueLane.key);
    assertEquals("Venue Lane score", 190, venueLane.score);
  }

}
