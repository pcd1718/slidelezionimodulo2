package pcd2017.threads.lab1.solution;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pcd2017.threads.lab1.bowling.GameRecord;

public class ScoreReaderTest {

  @Test
  public void testParse() {
    String input = "Jeffrey Leboski|Hollywood Star Lanes|4|8/9/72XXX3/819/XX5";
    GameRecord gameRecord = ScoreReader.parse(input);
    assertEquals("Name", "Jeffrey Leboski", gameRecord.player);
    assertEquals("Venue", "Hollywood Star Lanes", gameRecord.venue);
    assertEquals("Lane", 4, gameRecord.lane);
    assertEquals("Name", "8/9/72XXX3/819/XX5", gameRecord.score);

    System.out.println(">>>>>");
  }

}
