package pcd2017.threads.lesson5;

import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;

import org.junit.Test;

public class Example1Test {

  @Test
  public void test() {
    Divisors div = new Divisors();
    CandidateNumber div2 = div.apply(2);
    assertEq(div2, 2);
    CandidateNumber div6 = div.apply(6);
    assertEq(div6, 2, 3);
    CandidateNumber div37 = div.apply(37);
    assertEq(div37);
    CandidateNumber div1024 = div.apply(1024);
    assertEq(div1024, 2, 4, 8, 16, 32, 64, 128, 256, 512);
  }

  private void assertEq(CandidateNumber actual, int... expected) {
    assertEquals(actual.divisors.stream().map((i) -> i.toString()).collect(
        Collectors.joining(",")), expected.length, actual.divisors.size());
    for (int i = 0; i < expected.length; i++)
      assertEquals((Integer) expected[i], actual.divisors.get(i));

  }

}
